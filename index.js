const form = document.querySelector('.password-form');
const btnFirstEye = document.querySelector('.first-eye');
const btnSecondEye = document.querySelector('.second-eye');
const btnSubmit = document.querySelector('.btn');
const inputInvalid = document.querySelector('.input-invalid');




btnFirstEye.addEventListener('click', (evt) => {

    if (btnFirstEye.classList.contains('fa-eye')){

        btnFirstEye.classList.replace('fa-eye', 'fa-eye-slash');

        form.pass1.type = 'text';

    } else {
        btnFirstEye.classList.replace('fa-eye-slash', 'fa-eye');
        form.pass1.type = 'password';
    }

})

btnSecondEye.addEventListener('click', (evt) => {

    if (btnSecondEye.classList.contains('fa-eye')){

        btnSecondEye.classList.replace('fa-eye', 'fa-eye-slash');

        form.pass2.type = 'text';

    } else {
        btnSecondEye.classList.replace('fa-eye-slash', 'fa-eye');
        form.pass2.type = 'password';
    }

})

btnSubmit.addEventListener('click', (evt) => {

    evt.preventDefault();

    if (form.elements.pass1.value === form.elements.pass2.value) {
        form.elements.pass1.style.borderColor = '#00FF7F';
        form.elements.pass2.style.borderColor = '#00FF7F';
        alert('You are welcome');
    } else {
        form.elements.pass1.style.borderColor = '#ff0000';
        form.elements.pass2.style.borderColor = '#ff0000';

        inputInvalid.classList.remove('unvisible');

    }
})

console.log(form);